# GOF - Criacional - Abstract Factory
Este projeto tem o intuito de estudar o padrão `Abstract Factory` do grupo `Criacional` dos padrões de projeto GOF.

[Referência](https://refactoring.guru/pt-br/design-patterns/abstract-factory)

Neste projeto foi implementado um `gerador para a mensagem de notificação do alistamento do exército`.

# Docker
## Criar a imagem
```bash
docker-compose build --no-cache # Apenas uma vez
```

## Entrar no terminal do container
```bash
docker-compose run --rm workspace bash
```

# Instalar dependências composer
```bash
composer install
```

# Exemplos
```bash
php exemplos/app.php
```
> Será solicitada a idade que o usuário completará no ano atual, informe e pressione `ENTER`.

# Testes Automatizados
```bash
bin/phpunit --debug --coverage-text
```