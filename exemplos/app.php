<?php

require 'vendor/autoload.php';

$idade = readline('Quantos anos você completa em ' . date('Y') . ': ');

if ($idade < 18) {
    $mensagemAlistamento = new AbstractFactory\Fabrica\MensagemAlistamentoMenor18Anos();
} else {
    $mensagemAlistamento = new AbstractFactory\Fabrica\MensagemAlistamento18Anos();
}

$mensagemHomem = $mensagemAlistamento->criarMensagemHomem();
$mensagemMulher = $mensagemAlistamento->criarMensagemMulher();

echo PHP_EOL . "Mensagem para o sexo 'masculino': {$mensagemHomem->mensagemHomem()}" . PHP_EOL;
echo PHP_EOL . "Mensagem para o sexo 'feminino': {$mensagemMulher->mensagemMulher()}" . PHP_EOL;
