<?php

namespace AbstractFactory\Produto;

class MensagemMulherMenor18Anos implements MensagemMulherInterface
{
    public function mensagemMulher(): string
    {
        return 'A senhora ainda não tem idade para o alistamento.';
    }
}