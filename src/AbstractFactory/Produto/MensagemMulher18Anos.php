<?php

namespace AbstractFactory\Produto;

class MensagemMulher18Anos implements MensagemMulherInterface
{
    public function mensagemMulher(): string
    {
        return 'O seu alistamento é opcional.';
    }
}