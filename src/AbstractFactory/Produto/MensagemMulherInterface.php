<?php

namespace AbstractFactory\Produto;

interface MensagemMulherInterface
{
    public function mensagemMulher(): string;
}