<?php

namespace AbstractFactory\Produto;

class MensagemHomem18Anos implements MensagemHomemInterface
{
    public function mensagemHomem(): string
    {
        return 'O seu alistamento é obrigatório.';
    }
}