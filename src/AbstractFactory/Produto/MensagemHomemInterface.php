<?php

namespace AbstractFactory\Produto;

interface MensagemHomemInterface
{
    public function mensagemHomem(): string;
}