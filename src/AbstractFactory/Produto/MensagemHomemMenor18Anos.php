<?php

namespace AbstractFactory\Produto;

class MensagemHomemMenor18Anos implements MensagemHomemInterface
{
    public function mensagemHomem(): string
    {
        return 'O senhor ainda não tem idade para o alistamento.';
    }
}