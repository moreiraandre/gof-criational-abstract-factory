<?php

namespace AbstractFactory\Fabrica;

use AbstractFactory\Produto\{
    MensagemHomemInterface,
    MensagemHomemMenor18Anos,
    MensagemMulherInterface,
    MensagemMulherMenor18Anos
};

class MensagemAlistamentoMenor18Anos implements MensagemAlistamentoInterface
{
    public function criarMensagemHomem(): MensagemHomemInterface
    {
        return new MensagemHomemMenor18Anos();
    }

    public function criarMensagemMulher(): MensagemMulherInterface
    {
        return new MensagemMulherMenor18Anos();
    }
}