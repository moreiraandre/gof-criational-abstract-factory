<?php

namespace AbstractFactory\Fabrica;

use AbstractFactory\Produto\{MensagemHomemInterface, MensagemMulherInterface};

interface MensagemAlistamentoInterface
{
    public function criarMensagemHomem(): MensagemHomemInterface;
    public function criarMensagemMulher(): MensagemMulherInterface;
}