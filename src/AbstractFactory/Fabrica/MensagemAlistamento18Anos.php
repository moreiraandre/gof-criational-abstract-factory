<?php

namespace AbstractFactory\Fabrica;

use AbstractFactory\Produto\{
    MensagemHomem18Anos,
    MensagemHomemInterface,
    MensagemMulher18Anos,
    MensagemMulherInterface
};

class MensagemAlistamento18Anos implements MensagemAlistamentoInterface
{
    public function criarMensagemHomem(): MensagemHomemInterface
    {
        return new MensagemHomem18Anos();
    }

    public function criarMensagemMulher(): MensagemMulherInterface
    {
        return new MensagemMulher18Anos();
    }
}