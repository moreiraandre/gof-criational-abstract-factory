<?php

namespace Tests\Feature;

use AbstractFactory\Fabrica\MensagemAlistamentoMenor18Anos;
use PHPUnit\Framework\TestCase;

class MensagemMenor18Test extends TestCase
{
    public function testSucesso()
    {
        $servicoFabrica = new MensagemAlistamentoMenor18Anos();
        $mensagemHomem = $servicoFabrica->criarMensagemHomem();
        $mensagemMulher = $servicoFabrica->criarMensagemMulher();

        $this->assertEquals($mensagemHomem->mensagemHomem(), 'O senhor ainda não tem idade para o alistamento.');
        $this->assertEquals($mensagemMulher->mensagemMulher(), 'A senhora ainda não tem idade para o alistamento.');
    }
}